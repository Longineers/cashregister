<?php
class StaffMember {
  private $name;
  private $discountPercent = 0;
  
  function __construct( $p_sName, $p_iDiscountPercent = 0 ) {
    $this->name            = $p_sName;
    $this->discountPercent = $p_iDiscountPercent;
  }
  
  function getName() {
    return $this->name;
  }
  
  function getDiscountPercent() {
    return $this->discountPercent;
  }
  
}