<?php
class CashRegister {
  private $total           = 0;
  private $lastTransaction = 0;
  
  function add( $p_val ) {
    $this->total          += $p_val;
    $this->lastTransaction = $p_val;
  }
  
  function getTotal() {
    return $this->total;
  }
  
  function getLastTransaction() {
    return $this->lastTransaction;
  }
  
  function voidLastTransaction() {
    $this->total -= $this->lastTransaction;
  }
  
  function scan( $p_item, $p_quantity = 1 ) {
    switch ( $p_item ) {
      case 'eggs':
        $this->add( 0.98 * $p_quantity );
        break;
      case 'milk':
        $this->add( 1.23 * $p_quantity );
        break;
      case 'magazine':
        $this->add( 4.99 * $p_quantity );
        break;
      case 'chocolate':
        $this->add( 0.45 * $p_quantity );
        break;
      default:
        break;
    }
  }
  
  function applyStaffDiscount( $p_oStaffMember ) {
    $this->total -= $this->total * ( $p_oStaffMember->getDiscountPercent() / 100 );
  }  
}