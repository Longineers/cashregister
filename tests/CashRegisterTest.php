<?php

require_once '../src/CashRegister.php';
class CashRegisterTest extends PHPUnit_Framework_TestCase {
  private $oCashRegister;
  
  function setUp() {
    parent::setUp();
    $this->oCashRegister = new CashRegister();
  }
  
  /**
  * @test
  */
  function CashRegisterHasPropterty() {
    $this->assertClassHasAttribute( 'total', 'CashRegister' );
  }
  
  /**
  * @test
  */
  function CashRegisterAdd_TwoItems_SumTotal() {
    $iExpectTotal  = 2.1;
    
    $this->oCashRegister->add( 1.1 );
    $this->oCashRegister->add( 1 );
    $this->assertEquals( $iExpectTotal, $this->oCashRegister->getTotal() );
  }
  
  /**
  * @test
  */
  function CashRegisterScan_FourItems_SumTotal() {
    $iExpectedTotal = 0.98 + 1.23 + 4.99 + ( 0.45 * 2);
    $this->oCashRegister->scan( 'eggs'     , 1 );
    $this->oCashRegister->scan( 'milk'     , 1 );
    $this->oCashRegister->scan( 'magazine' , 1 );
    $this->oCashRegister->scan( 'chocolate', 2 );
    
    $this->assertEquals( $iExpectedTotal, $this->oCashRegister->getTotal() );
  }
  
  /**
  * @test
  */
  function CashRegisterLastTransaction_TwoItems_LastItemUpdated() {
    $this->oCashRegister->add( 123 );
    $this->assertEquals( 123, $this->oCashRegister->getLastTransaction() );
    $this->oCashRegister->add( 3 );
    $this->assertEquals( 3, $this->oCashRegister->getLastTransaction() );
  }
  
  /**
  * @test
  */
  function CashRegisterVoidLastTransaction_FourItems_IgnoreLastScannedItem() {
    $this->oCashRegister->scan( 'eggs'     , 1 );
    $this->oCashRegister->scan( 'milk'     , 1 );
    $this->oCashRegister->scan( 'magazine' , 1 );
    $this->oCashRegister->scan( 'chocolate', 4 );
    
    $iExpect = ( 1 * 0.98 ) + 
               ( 1 * 1.23 ) + 
               ( 1 * 4.99 ) + 
               ( 4 * 0.45 ); 
    $this->assertEquals( $iExpect
                       , $this->oCashRegister->getTotal() 
                       );
    
    $this->oCashRegister->voidLastTransaction();
    
    $iExpect = ( 1 * 0.98 ) + 
               ( 1 * 1.23 ) + 
               ( 1 * 4.99 );
    $this->assertEquals( $iExpect
                       , $this->oCashRegister->getTotal() 
                       );
    
    $iExpect = ( 4 * 0.45 );
    $this->assertEquals( $iExpect
                       , $this->oCashRegister->getLastTransaction() 
                       );
  }
}